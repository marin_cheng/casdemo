package com.cas.client.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Description
 * @Author MarinCheng
 * @Create 2019/12/4 13:32
 * @Version 1.0.0
 */
@Controller
public class IndexController {

    @GetMapping("/")
    @ResponseBody
    public String index() {
        return "index";
    }
}